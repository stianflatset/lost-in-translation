import { Link } from "react-router-dom";
import React from "react";
import logo_hello from "../../images/logo_hello.png";
import AppContainer from "../../hoc/AppContainer";

//If the user types in a url that does not exists this function is triggered.
function NotFound() {
  return (
    <AppContainer>
      <div className="row mt-5">
        <div className="col">
          <h1>Ooops!</h1>
          <h3>This page does not exists</h3>
          <p>You are literally lost in translation</p>
          <Link to="/">Take me to safe ground 😅</Link>
        </div>

        <div className="col">
          <img
            src={logo_hello}
            alt="upsie"
            className="thumbnail img-responsive w-75 p-3"
          />
        </div>
      </div>
    </AppContainer>
  );
}

export default NotFound;
