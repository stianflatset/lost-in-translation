export const ACTION_SEARCHED_FETCHING = '[searched] FETCHING'
export const ACTION_SEARCHED_SET = '[searched] SET'
export const ACTION_SEARCHED_ERROR = '[searched] ERROR'

export const searchedFetchingAction = userId => ({
    type: ACTION_SEARCHED_FETCHING,
    payload: userId
})

export const searchedSetAction = searched => ({
    type: ACTION_SEARCHED_SET,
    payload: searched
})

export const searchedErrorAction = error => ({
    type: ACTION_SEARCHED_ERROR,
    payload: error
})