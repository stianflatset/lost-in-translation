import { ACTION_SEARCHED_ERROR, ACTION_SEARCHED_FETCHING, ACTION_SEARCHED_SET } from "../actions/searchedActions"

const initialState = {
    searchedError: '',
    searchedFetching: false,
    searched: []
}

export const searchedReducer = (state = {...initialState}, action) => {
    switch(action.type) {
        case ACTION_SEARCHED_FETCHING:
            return {
                ...state,
                searchedError: '',
                searchedFetching: true
            }
        case ACTION_SEARCHED_SET:
            return {
                ...state,
                searchedError: '',
                searched: [...action.payload]
            }
        case ACTION_SEARCHED_ERROR:
            return {
                ...state,
                searchedError: action.payload,
                searchedFetching: false
            }
        default:
            return state
    }
}